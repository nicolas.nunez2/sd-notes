#from flask import Flask
#from flask_restful import Resource, Api
from kafka import KafkaConsumer
import json

# Inicializaciones de Flask
#app = Flask(__name__)
#api = Api(app)

# Inicializacion Kafka
consumer = KafkaConsumer(
        'login',
        bootstrap_servers='localhost:9092',
        value_deserializer=lambda v: json.loads(v.decode('utf-8')),
        group_id='hola',
        auto_offset_reset='earliest'
        )

# print(dir(consumer))

for message in consumer:
    print(message.value)

#def refreshConsumer():
#    for 

# Endpoint /blocked [GET]
#class Blocked(Resource):
#    def get(self):
#        refreshConsumer()
#        return data, 201

#api.add_resource(Blocked, '/blocked')

#if __name__ == "__main__":
#    app.run(port=3000, debug=True)
