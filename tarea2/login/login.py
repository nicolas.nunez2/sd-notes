from flask import Flask
from flask_restful import Resource, Api, reqparse
from kafka import KafkaProducer
import json
import time

# Inicializaciones de Flask
app = Flask(__name__)
api = Api(app)

# Parsers para el Endpoint
parser = reqparse.RequestParser()
parser.add_argument('user')
parser.add_argument('pass')

# Inicializacion Kafka
# Enviar data serializada como JSON
producer = KafkaProducer(value_serializer=lambda v: json.dumps(v).encode('utf-8'), bootstrap_servers='localhost:9092')


# Endpoint /login [POST]
class Login(Resource):
    def post(self):
        args = parser.parse_args()
        if (args['user'] != None and args['pass'] != None):
            # Se envia el mensaje hacia el topico login
            producer.send('login', {
                'user': args['user'],
                'pass': args['pass'],
                'time': time.time()
                })
            return {'status': 'ok'}, 201
        else:
            return {'status': 'error'}, 500
api.add_resource(Login, '/login')

if __name__ == "__main__":
    app.run(debug=True)
